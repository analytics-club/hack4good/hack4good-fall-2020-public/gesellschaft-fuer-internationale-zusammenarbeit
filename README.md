# GIZ

## Project description
The final goal of this project was to identify drivers in soil and weather data influencing the crop yield in farms within Eastern Zambia. Therefore, we retrieved soil data from [soilgrids.org](soilgrids.org) and weather data from the [ERA5 reanalysis](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview). The land productivity data was derived from survey data from the years 2016 until 2019 which was collected by GIZ.

## Data
Since we are not allowed to share the data from GIZ in GitLab, I added the structure from the shared polybox folder under `/data/GIZ_input_files/`. Simply manually add the files from the polybox (if you are granted access) to the corresponding folder under `/data/GIZ_input_files/`. The data files will then automatically be ignored from the commits.

Soil and weather data are accessible publicly and are automatically downloaded in the code via the API.

## Environment
To set up the environment with conda, run:
```
conda env create -f environment.yml
```

Then simply activate:
```
conda activate H4G_env
```