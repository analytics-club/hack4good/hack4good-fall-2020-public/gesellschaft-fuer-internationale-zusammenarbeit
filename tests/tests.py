import unittest

from scripts.Preprocessing.cleaning_16 import *


class MyTestCase(unittest.TestCase):

    def test_2016_for_correctness(self):
        '''
        Simple test, that reads in the created cleaned csv file for 2016 and checks whether the existing columns match the should have columns from the Google Drive.
        '''
        CSV_columns = pd.read_csv("../data/clean_survey_data/2016_clean.csv").columns.tolist()

        try:
            df_final_variables = pd.read_csv(
                "../data/GIZ_input_files/data_for_cleaning/Questionaire - Manual Inspection of Questions - Final variable mapping.csv")
            should_columns = pd.read_csv(
                "../data/GIZ_input_files/data_for_cleaning/Questionaire - Manual Inspection of Questions - Final variable mapping.csv")[
                                 "Final column name"].values.tolist() + ["date", "gps_alt"]

        except:
            raise ValueError(
                "Please download Questionaire - Manual Inspection of Questions - Final variable mapping.csv from the Google Drive")

        self.assertListEqual(CSV_columns, should_columns)

if __name__ == '__main__':
    unittest.main()
