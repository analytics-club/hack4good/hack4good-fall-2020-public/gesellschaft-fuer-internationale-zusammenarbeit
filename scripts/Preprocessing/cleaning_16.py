import pandas as pd
import numpy as np


def map_villages_to_coordinates(df):
    """ 
    Map the village names to the corresponding GPS coordinates
    
    """
    # Read in the file with all avaliable villages ("AV-file")
    df_villages = pd.read_excel("../../data/GIZ_input_files/data_for_cleaning/VILLAGE LOCATION COMACO AREAS-East(1).xlsx")

    # Define set of all avalibale villages (with only lower chars and whitespaces removed)
    avalibale_villages = set(df_villages["village"].str.lower().replace(regex=r"\s+", value="").values)
    avalibale_villages -= {"n", "m", "\\n", "\n", "", np.NaN}

    # Define mappings from villages in AV-file to their gps attributes (could in principle also be done with a JOIN)
    dict_latitudes = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["latitude"]))
    dict_longitudes = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["longitude"]))
    dict_altitudes = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["elevation"]))
    dict_accuracy = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["accuracy"]))

    del dict_latitudes[np.NaN], dict_longitudes[np.NaN], dict_altitudes[np.NaN], dict_accuracy[np.NaN]

    # Create a mapping between village names in 2016 file and AV-file
    df["village_name_processed"] = df["village"].str.lower().replace(regex=r"\s+", value="")

    village_mapping = dict()
    villages_not_found = []
    alternatives_checked = []

    for village_name in sorted(set(df["village_name_processed"])):
        if village_name in avalibale_villages:
            village_mapping[village_name] = village_name
            continue

        # Start filtering: 
        # Search for certain keywords/characters and evaluate the substring before this keyword/charater
        potential_village_names_list = []
        for keyword in ["farm", "village", "settlement", "compound", "school", "section", "1", "2", "("]:
            if keyword in village_name:
                index = village_name.find(keyword)
                village_name_altered = village_name[:index]
                potential_village_names_list.append(village_name_altered)

        # Search for match with in AV-file villages
        found_village = False
        for potential_name in potential_village_names_list:
            if potential_name in avalibale_villages:
                village_mapping[village_name] = potential_name
                found_village = True
                break

        # Print villages that could not be mapped
        if not found_village:
            villages_not_found.append(village_name)
            alternatives_checked.append(potential_village_names_list)

    # Apply mapping from 2016 file to coordinates from AV-file
    df["village_name_avaliable"] = df["village_name_processed"].map(village_mapping)
    df["gps__latitude"] = df["village_name_avaliable"].map(dict_latitudes)
    df["gps__longitude"] = df["village_name_avaliable"].map(dict_longitudes)
    df["gps__accuracy"] = df["village_name_avaliable"].map(dict_accuracy)
    df["gps__altitude"] = df["village_name_avaliable"].map(dict_altitudes)

    print(f"{df['gps__latitude'].notnull().sum()}/{len(df['gps__latitude'])} rows in 2016 file have a GPS value")
    return df

def unify_units_16(df):
    """ 
    Unify columns with varying units
    """

    # Unit conversions
    area_conv = {
        1: 1,  # "hectare",
        2: 0.4046856422,  # "acre",
    }

    vol_conv = {
        1: 1,  # "kg",
        4: 25,  # "bag_25",
        5: 50,  # "bag_50",
        7: 15,  # "box_15"
    }

    mat_conv = {
        1: 1,  # "kg",
        2: 1000,  # "ton",
        3: 1,  # "litre",
        4: 1000,  # "cubic_meter",
        5: 0.5,  # "tasa",
        6: 25,  # "bag_25",
        7: 50,  # "bag_50",
    }

    # Fertilizer variables to convert
    to_convert = {
        "fert_am_1": "fert_un_1",
        "fert_am_2": "fert_un_2",
        "man_am_1": "man_un_1",
        "man_am_2": "man_un_2",
        "herb_am_1": "herb_un_1",
        "herb_am_2": "herb_un_2",
        "pest_am_1": "pest_un_1",
        "pest_am_2": "pest_un_2",
        "fung_am_1": "fung_un_1",
        "fung_am_2": "fung_un_2"
    }

    # Variable from questionaire for this does not exist.
    # df["land_amount"] = df["land_amount"]*(df["land_amount_title_unit"].map(area_conv))

    # unify units on  production area:
    df["area_1_1"] = df["area_1_1"] #* (df["ar_1_1_un"].map(area_conv))
    df["area_2_1"] = df["area_2_1"] #* (df["ar_2_1_un"].map(area_conv))

    # unify units on production volume:
    df["vol_1_1"] = df["vol_1_1"] * (df["foc_1_un"].map(vol_conv))
    df["vol_2_1"] = df["vol_2_1"] * (df["foc_2_un"].map(vol_conv))

    # unify units on fertilizer, manure, insect., herb., fung.
    for variable, unit in to_convert.items():
        df[variable] = df[variable] * (df[unit].map(mat_conv))

    return df


def map_focus_vars_16(df):
    """
    Map focus crop variables to groundnut (gr) and soy (so) variables
    
    """

    df["soybeans"] = (df["focusvc_1"] == 36) | (df["focusvc_2"] == 36) | (df["focusvc_3"] == 36)
    df["groundnuts"] = (df["focusvc_1"] == 37) | (df["focusvc_2"] == 37) | (df["focusvc_3"] == 37)

    cr_abbr = {36: "so", 37: "gr"}

    for crop_code, crop in cr_abbr.items():
        for j in [1, 2, 3]:
            comb_vars = {
                f"{crop}_area": f"area_{j}_1",
                f"{crop}_vol": f"vol_{j}_1",
                f"{crop}_chemical": f"fert_{j}",
                f"{crop}_manure": f"man_{j}",
                f"{crop}_insecticides": f"pest_{j}",
                f"{crop}_herbicides": f"herb_{j}",
                f"{crop}_fungicides": f"fung_{j}",
                f"{crop}_chemical_amount": f"fert_am_{j}",
                f"{crop}_manure_amount": f"man_am_{j}",
                f"{crop}_insecticides_amount": f"pest_am_{j}",
                f"{crop}_herbicides_amount": f"herb_am_{j}",
                f"{crop}_fungicides_amount": f"fung_am_{j}",
                f"{crop}_male_household": f"ma_{j}",
                f"{crop}_female_household": f"fe_{j}",
            }

            crop_mask = (df[f"focusvc_{j}"] == crop_code)

            for new_name, old_name in comb_vars.items():
                df.loc[crop_mask, new_name] = df.loc[crop_mask, old_name]

    # Varibles inserted manually, since they do not concide with questionaire
    df["gr_male_household_workdays"] = df["inp_ma_1_peanut"]
    df["so_male_household_workdays"] = df["inp_ma_1_soy"]
    df["gr_female_household_workdays"] = df["inp_fe_1_peanut"]
    df["so_female_household_workdays"] = df["inp_fe_1_soy"]
    return df


def clean_16_survey(df):
    """
    Clean '16 survey for combined df
    
    Missing variables: 
    - gps_lat, gps_lon, district, radio, tech_devices, internet
    - so_male_household_workdays, so_male_household_workdays (derived from data)
    
    """

    # Map the village names to the corresponding GPS coordinates
    df = map_villages_to_coordinates(df)

    # unify units
    df = unify_units_16(df)

    # map focus crop specific variables
    df = map_focus_vars_16(df)

    # select columns to keep and create combined df
    variable_mapping = {"interview__id": "id",
                        "gps__latitude": "gps_lat",
                        "gps__longitude": "gps_lon",
                        "gps__altitude": "gps_alt",
                        "crop_rot": "crop_rotation",
                        # "agroforestry": "NA",
                        "internet": "internet",
                        # "radio_comaco": "NA",
                        }

    df = df.rename(columns=variable_mapping)

    # Radio
    df["radio"] = (df["hh_assets_5"] > 0)

    # Tech devices (Mobile phone, Radio, Television, Computer)
    df["tech_devices"] = (df["hh_assets_4"] > 0) | (df["hh_assets_5"] > 0) | (df["hh_assets_6"] > 0) | (
                df["hh_assets_7"] > 0)

    # fill missing columns with NaN
    missing_columns = ["land_amount", "district", "agroforestry", "radio_comaco", "so_male_hired", "so_female_hired",
                       "gr_male_hired", "gr_female_hired", "district"]

    for col in missing_columns:                   
        df[col] = np.nan

    # Keep only variables from final variable table online in Google Sheets (have to download - did not want to keep this in the Git repository due to information about the surveys)
    try:
        df_final_variables = pd.read_csv("../../data/GIZ_input_files/data_for_cleaning/Questionaire - Manual Inspection of Questions - Final variable mapping.csv")
    except:
        raise ValueError("Please download Questionaire - Manual Inspection of Questions - Final variable mapping.csv from the Google Drive")
    
    columns_to_keep = list(df_final_variables["Final column name"]) + ["date", "gps_alt"]
    return df[columns_to_keep]


if __name__ == "__main__":
    # Read and split the 2016 dataframe from the combined 2016/2018 CSV table
    df_2016_2018_raw = pd.read_csv("../../data/GIZ_input_files/answers/GIC Zambia_Data_HH_EP_ 2016_2018.csv", encoding="cp437", low_memory=False)
    df_2016_raw = df_2016_2018_raw.loc[df_2016_2018_raw["date"].str.contains("2016"), :].reset_index(drop=True)
    df_2016 = df_2016_raw.copy()

    # Clean 2016 dataframe & write to CSV
    df_2016_clean = clean_16_survey(df_2016.copy())
    df_2016_clean.to_csv("../../data/clean_survey_data/2016_clean.csv", index=False)
