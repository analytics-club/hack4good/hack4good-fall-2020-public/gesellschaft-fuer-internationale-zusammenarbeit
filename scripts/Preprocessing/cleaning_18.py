import pandas as pd
import numpy as np
import os

def unit_convert_18(df):
        # Unit conversions
    area_conv = {
        1: 1,  # "hectare",
        2: 0.4046856422,  # "acre",
        3: 0.0001,   # "square meter"
        4: 0.000083612736,    # "yard"
        5: 0.25 # "timad/lima"
    }

    vol_conv = {
        1: 1,  # "kg",
        4: 25,  # "bag_25",
        5: 50,  # "bag_50",
        7: 15,  # "box_15"
    }

    for focus_group in [1, 2, 3]:
        for season in [1, 2, 3]:
            #unify units in production area
            df[f"ar_{focus_group}_{season}"] = df[f"ar_{focus_group}_{season}"] * (df[f"ar_{focus_group}_{season}_un"].map(area_conv).fillna(1))
            #unify units in production volumn
            df[f"vol_{focus_group}_{season}"] = df[f"vol_{focus_group}_{season}"] *(df[f"foc_{focus_group}_un"].map(vol_conv).fillna(1))
    return df

def map_focus_crops_18(df):
    df["so"] = (df["focusvc_1"] == 36) | (df["focusvc_2"] == 36) | (df["focusvc_3"] == 36)
    df["gr"] = (df["focusvc_1"] == 37) | (df["focusvc_2"] == 37) | (df["focusvc_3"] == 37)
    return df

def map_district_18(df):
    dis_conv = {
            47: "Katete", 
            48: "Sinda", 
            49: "Petauke", 
            50: "Monze", 
            51: "Choma", 
            52: "Mazabuka", 
            53: "Namwala", 
            54: "Kalomo",
            -999999999: 0
    }
    df["district"] = df["region"].map(dis_conv)
    return df

def map_tech_devices_18(df):
    #04 Mobile phone 05 Radio 06 Television 07 Computer
    df["tech_devices"] = (df["hh_assets__4"] == 1) | (df["hh_assets__5"] == 1) |(df["hh_assets__6"] == 1) | (df["hh_assets__7"] == 1)
    return df

def map_internet_18(df):
    df["internet"] = (df["internet"] != 0 )
    return df

def crops_production_18(df):
    #produnction area
    for focus_group in [1,2,3]:
        df[f"ar_{focus_group}"] = sum(df[f"ar_{focus_group}_{season}"].fillna(0) for season in [1,2,3])
        df[f"vol_{focus_group}"] = sum(df[f"vol_{focus_group}_{season}"].fillna(0) for season in [1,2,3])
    return df

def map_crops_production_18(df):
    cr_abbr = {36: "so", 37: "gr"}
    for crop_code, crop in cr_abbr.items():
        df[crop + "_area"] = 0
        df[crop + "_vol"] = 0
        for i in df.index:
            if df.loc[i, crop] == 0:
                df.loc[i, crop+"_area"] = np.nan
                df.loc[i, crop+"_vol"] = np.nan
            else:
                for nbr in ["1","2","3"]:
                    if df.loc[i, "focusvc_"+ nbr] == crop_code:
                        df.loc[i, crop+"_area"] += df.loc[i, "ar_"+nbr]
                        df.loc[i, crop + "_vol"] += df.loc[i, "vol_"+nbr]
    return df
    
def map_labour_18(df):
    cr_abbr = {36: "so", 37: "gr"}
    for crop_code, crop in cr_abbr.items():
        df[crop + "_male_household"] = 0
        df[crop + "_female_household"] = 0
        #df[crop + "_male_household_workdays"] = 0
        #df[crop + "_female_household_workdays"] = 0
        df[crop + "_male_hired"] = 0
        df[crop + "_female_hired"] = 0
        for i in df.index:
            if df.loc[i, crop] == 0:
                df.loc[i, crop + "_male_household"] = np.nan
                df.loc[i, crop + "_female_household"] = np.nan
                #df[crop + "_male_household_workdays"][i] = "NA"
                #df[crop + "_female_household_workdays"][i] = "NA"
                df.loc[i, crop + "_male_hired"] = np.nan
                df.loc[i, crop + "_female_hired"] = np.nan
            else:
                for nbr in ["1","2","3"]:
                    if df.loc[i, "focusvc_"+ nbr]== crop_code:
                        df.loc[i, crop + "_male_household"] += df["ma_"+nbr][i]+df["you_ma_"+nbr][i]
                        df.loc[i, crop + "_female_household"] += df["fe_"+nbr][i]+df["you_fe_"+nbr][i]
                         #df[crop + "_male_household_workdays"][i] += df["inp_ma_"+nbr][i]+df["you_inp_ma_"+nbr][i]
                         #df[crop + "_female_household_workdays"][i] += df["inp_fe_"+nbr][i]+df["you_inp_fe_"+nbr][i]
                        df.loc[i, crop + "_male_hired"] += df["hi_ma_nr_"+nbr][i]+df["hi_you_ma_nr_"+nbr][i]
                        df.loc[i, crop + "_female_hired"] += df["hi_fe_nr_"+nbr][i]+df["hi_you_fe_nr_"+nbr][i]
    return df
    
def map_others_18(df):
    variable_mapping = {"interview__id": "id",
                        "gps__latitude": "gps_lat",
                        "gps__longitude": "gps_lon",
                        "crop_rot": "crop_rotation",
                        "inno_zam__74": "agroforestry",
                        "hh_assets__5": "radio",
                        "inno_zam__76": "radio_comaco",
                        "gr": "groundnuts",
                        "so":"soybeans"
                        }
    df = df.rename(columns=variable_mapping)
    return df

def clean_18_survey(df_raw):
    df = df_raw.copy()
    df["gps_alt"] = df["gps__altitude"]
    df = unit_convert_18(df)
    df = map_focus_crops_18(df)
    df = map_district_18(df)
    df = map_tech_devices_18(df)
    df = map_internet_18(df)
    df = crops_production_18(df)
    df = map_crops_production_18(df)
    df = map_labour_18(df)
    df = map_others_18(df)

    
    # missing columns
    missing_columns = ['land_amount','gr_male_household_workdays','so_male_household_workdays','gr_female_household_workdays','so_female_household_workdays','so_manure_amount', 'gr_chemical', 'gr_insecticides_amount', 'so_fungicides', 
    'so_chemical_amount', 'gr_chemical_amount', 'so_fungicides_amount', 'gr_herbicides', 'so_herbicides_amount', 
    'so_herbicides', 'gr_herbicides_amount', 'so_chemical', 'gr_fungicides', 'gr_manure', 
    'gr_fungicides_amount', 'so_insecticides_amount', 'gr_insecticides', 'so_insecticides', 'gr_manure_amount', 
    'so_manure']

    for col in missing_columns:                   
        df[col] = np.nan

    #columns to keep
    df_final_variables = pd.read_csv("../../data/GIZ_input_files/data_for_cleaning/Questionaire - Manual Inspection of Questions - Final variable mapping.csv")
    columns_to_keep = df_final_variables["Final column name"].tolist()
    columns_to_keep.append("gps_alt")
    df = df[columns_to_keep]
    return df


if __name__ == "__main__":
    df_2016_2018_raw = pd.read_csv("../../data/GIZ_input_files/answers/GIC Zambia_Data_HH_EP_ 2016_2018.csv", 
                                   encoding="cp437", low_memory=False)
    df_2018_raw = df_2016_2018_raw.loc[df_2016_2018_raw["date"].str.contains("2018"), :].reset_index(drop=True)
    df_2018 = df_2018_raw.copy()

    #df_2018 = df_2018.fillna(0)
    df_2018_clean = clean_18_survey(df_2018.copy())

    df_2018_clean.to_csv('../../data/clean_survey_data/2018_clean.csv', index=False)

    
            
        
  
        

        


            



    
    

