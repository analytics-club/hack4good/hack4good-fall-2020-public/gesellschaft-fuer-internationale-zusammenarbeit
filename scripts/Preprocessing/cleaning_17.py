import numpy as np
import pandas as pd
import math


def convert_col_unit(x_col, un_col, un_codes, un_convs):
    """
    Convert column unit
    Takes value and units column, and corresponding units and conversion dicts
    """
    return [x * un_convs[un_codes[un]] if not math.isnan(un) else float("nan") 
            for x, un in zip(x_col, un_col)]

            
def unify_units_17(df_2017):
    """ Unify columns with varying units """
    
    # unit conversions 
    area_un = {
        1: "hectare", 
        2: "acre", 
        4: "lima"
    }

    area_conv = {
        "acre": 0.4046856422, 
        "lima": 0.25, 
        "timad": 0.25, 
        "hectare": 1
    }

    vol_un = {
        1: "kg", 
        2: "litre", 
        3: "bag_unshelled_25", 
        4: "bag_shelled_25"
    }

    vol_conv = {
        "kg": 1, 
        "bag_unshelled_50": 50,
        "bag_unshelled_25": 25,
        "bag_shelled_50": 50,
        "bag_shelled_25": 25,
        "bag/ basket/ wheel barrow": 50,
        "bag": 75
    }

    mat_un = {
        1: "kg", 
        4: "ml", 
        2: "litre", 
        3: "bag", 
        12: "metre"
    }

    # not sure to which unit to convert to...
    mat_conv = {
        "kg": 0.001, 
        "ml": 1,
        "litre": 1, # assume 
        "bag": 25, 
        "metre": 1
    }

    to_convert = {
        "fert_am_1": "fert_un_1", 
        "fert_am_2": "fert_un_2",
        "man_am_1": "man_un_1", 
        "man_am_2": "man_un_2",
        "herb_am_1": "herb_un_1", 
        "herb_am_2": "herb_un_2",
        "pest_am_1": "pest_un_1", 
        "pest_am_2": "pest_un_2",
        "fung_am_1": "fung_un_1", 
        "fung_am_2": "fung_un_2"
    }
    
    # unify units on total land amount
    df_2017["land_amount"] = convert_col_unit(df_2017.land_amount_title, df_2017.land_amount_title_unit, area_un, area_conv)

    # unify units on  production area:
    df_2017["ar_1_1"] = convert_col_unit(df_2017.ar_1_1, df_2017.ar_1_1_un, area_un, area_conv)
    df_2017["ar_2_1"] = convert_col_unit(df_2017.ar_2_1, df_2017.ar_2_1_un, area_un, area_conv)

    #  unify units on production volume:
    df_2017["vol_1_1"] = convert_col_unit(df_2017.vol_1_1, df_2017.foc_1_un, vol_un, vol_conv)
    df_2017["vol_2_1"] = convert_col_unit(df_2017.vol_2_1, df_2017.foc_2_un, vol_un, vol_conv)

    #  unify units on fertilizer, manure, insect., herb., fung.
    for var in to_convert.keys():
        df_2017[var] = convert_col_unit(df_2017[var], df_2017[to_convert[var]], mat_un, mat_conv)
    
    return df_2017


def map_focus_vars_17(df_2017):
    """ Map focus crop variables to groundnut (gr) and soy (so) variables """
    
    focusvc_dicts = []
    cr_abbr = {"Groundnuts": "gr", "Soy": "so"}

    for i, row in df_2017.iterrows():
        row_dict = {"Groundnuts": False, "Soy": False}

        for j in [1, 2]:
            focus_crop = row[f"focusvc_{j}"]   
            # if there is no secondary crop, no values so break
            if focus_crop == "None":
                break 
    
            row_dict[focus_crop] = True
            crop = cr_abbr[focus_crop]

            comb_vars = {
                f"{crop}_area": f"ar_{j}_1",
                f"{crop}_vol": f"vol_{j}_1",
                f"{crop}_chemical": f"fert_{j}",
                f"{crop}_manure": f"man_{j}",
                f"{crop}_insecticides": f"pest_{j}",
                f"{crop}_herbicides": f"herb_{j}",
                f"{crop}_fungicides": f"fung_{j}",
                f"{crop}_chemical_amount": f"fert_am_{j}",
                f"{crop}_manure_amount": f"man_am_{j}",
                f"{crop}_insecticides_amount": f"pest_am_{j}",
                f"{crop}_herbicides_amount": f"herb_am_{j}",
                f"{crop}_fungicides_amount": f"fung_am_{j}",
                f"{crop}_male_household": f"ma_{j}",
                f"{crop}_female_household": f"fe_{j}",
                f"{crop}_male_hired": f"lab_hi_ma_{j}",
                f"{crop}_female_hired": f"lab_hi_fe_{j}",
            }

            for var in comb_vars.keys():
                row_dict.update({var: row[comb_vars[var]]})

            # derive male and female workdays from share of workforce 
            # (with assumption of equal work sharing)
            tot_workers_hh = row[f"ma_{j}"] + row[f"fe_{j}"]

            ma_w_days_hh = row[f"inp_{j}_10"] * row[f"ma_{j}"] / tot_workers_hh
            fe_w_days_hh = row[f"inp_{j}_10"] * row[f"fe_{j}"] / tot_workers_hh

            row_dict.update({f"{crop}_male_household_workdays": ma_w_days_hh,
                            f"{crop}_female_household_workdays": fe_w_days_hh})
            #break

        focusvc_dicts.append(row_dict)

    return pd.DataFrame(focusvc_dicts)


def clean_17_survey(df_2017):
    """
    Clean '17 survey for combined df
    
    Missing variables: 
    - gps_lat, gps_lon, district, radio, tech_devices, internet
    - so_male_household_workdays, so_male_household_workdays (derived from data)
    """
    
    # unify units
    df_2017 = unify_units_17(df_2017)
    
    # map focus crop specific variables
    focusvc_df_2017 = map_focus_vars_17(df_2017)
    
    # select columns to keep and create combined df
    df_2017_clean = df_2017[["KEY", "date", "crop_rot", "forest", "radio", "land_amount"]]
    df_2017_comb = pd.concat([df_2017_clean, focusvc_df_2017], axis = 1)

    # rename columns
    df_2017_comb = df_2017_comb.rename(columns = {"KEY": "id", 
                                                  'crop_rot':'crop_rotation',
                                                  'forest': 'agroforestry',
                                                  'radio': 'radio_comaco',
                                                  'Groundnuts': 'groundnuts',
                                                  'Soy': 'soybeans'
                                                   })
    # fill missing columns with NaN
    for var in ["village", "gps_lat", "gps_lon", "district", "radio", "tech_devices", "internet"]:
        df_2017_comb[var] = np.nan
        
    df_final_variables = pd.read_csv("../../data/GIZ_input_files/data_for_cleaning/Questionaire - Manual Inspection of Questions - Final variable mapping.csv")
    columns_to_keep = df_final_variables["Final column name"]
    df_2017_comb = df_2017_comb[columns_to_keep]
        
    return df_2017_comb


if __name__ == "__main__":
    ## load 2017 survey data
    df_2017_raw = pd.read_csv("../../data/GIZ_input_files/answers/GIC Zambia_Data_HH_EP_2017.csv", low_memory=False, 
                              parse_dates = ["date"])

    df_2017 = df_2017_raw.copy()
    
    df_2017_clean = clean_17_survey(df_2017)
    df_2017_clean.to_csv("../../data/clean_survey_data/2017_clean.csv", index=False)