import pandas as pd
import os
import numpy as np

from cleaning_16 import clean_16_survey
from cleaning_17 import clean_17_survey
from cleaning_18 import clean_18_survey
from cleaning_19 import clean_19_survey

from soil_data_mapping import map_soildata_to_GPS_location, download_all_soil_maps

from scripts.Preprocessing.cleaning_19 import filter_productivities

weather_path = "../../data/weather"
input_path = "../../data/GIZ_input_files/answers/"
output_path = "../../data/clean_survey_data/"


def read_surveys(input_path=input_path):
    """ Reads survey data and returns separate dataframe for each year. """
    df_2016_2018_raw = pd.read_csv(os.path.join(input_path, "GIC Zambia_Data_HH_EP_ 2016_2018.csv"), encoding="cp437",
                                   low_memory=False, parse_dates=["date"])
    df_2017_raw = pd.read_csv(os.path.join(input_path, "GIC Zambia_Data_HH_EP_2017.csv"), low_memory=False,
                              parse_dates=["date"])
    df_2019_raw = pd.read_csv(os.path.join(input_path, "GIC Zambia_Data_HH_EP_ 2019.csv"), low_memory=False,
                              parse_dates=["date"])

    # Split big dataset into the years 2016 and 2018 since the contained data is quite different
    df_2016_raw = df_2016_2018_raw.loc[df_2016_2018_raw["date"].dt.year == 2016, :].reset_index(drop=True)
    df_2018_raw = df_2016_2018_raw.loc[df_2016_2018_raw["date"].dt.year == 2018, :].reset_index(drop=True)
    return (df_2016_raw, df_2017_raw, df_2018_raw, df_2019_raw)


def prepare_survey_data():
    """ Clean all survey data and save combined csv """
    df_2016, df_2017, df_2018, df_2019 = read_surveys()

    # Clean each survey
    df_2016_clean = clean_16_survey(df_2016)
    df_2017_clean = clean_17_survey(df_2017)
    df_2018_clean = clean_18_survey(df_2018)
    df_2019_clean = clean_19_survey(df_2019)
    
    df_2016_clean["year"] = 2016
    df_2017_clean["year"] = 2017
    df_2018_clean["year"] = 2018
    df_2019_clean["year"] = 2019

    # Combine annual surveys
    df_comb = pd.concat([df_2016_clean, df_2017_clean, df_2018_clean, df_2019_clean], axis=0)

    # Land productivity (t / ha)
    df_comb["gr_land_prod"] = df_comb.gr_vol / (df_comb.gr_area * 1000)
    df_comb["so_land_prod"] = df_comb.so_vol / (df_comb.so_area * 1000)

    # Filter outliers (We only loose about 30-50 data points per crop)
    df_comb = filter_productivities(df_comb)

    # Decide weather village belongs to plateau or valley. 810 is best splitpoint from new data between valley and Plateau stations
    df_comb["plateau_or_valley"] = np.NaN
    df_comb.loc[df_comb["gps_alt"] >= 810, "plateau_or_valley"] = "plateau"
    df_comb.loc[df_comb["gps_alt"] < 810, "plateau_or_valley"] = "valley"

    return df_comb


if __name__ == "__main__":
    # Make sure your working directory is set to the current folder (=Preprocessing).
    # Otherwise all the relative links wont work.
    df_comb_clean = prepare_survey_data()

    # Add soil data information. Download the files first, if necessary.
    soil_data_downloaded = True
    if not soil_data_downloaded:
        download_all_soil_maps()

    # Keep only rows in Eastern Province and discard the dairy rows in Southern Province
    df_comb_clean = df_comb_clean.loc[df_comb_clean["gps_lon"] > 29, :]
    df_comb_clean = df_comb_clean.dropna(axis=0, subset=["gps_lat", "gps_lon"], how="any").reset_index(drop=True) # Drop all rows which do not have GPS data
    df_comb_clean.to_csv(os.path.join(output_path, "comb_clean.csv"), index=False)

    # Join df with soil data if wanted
    map_soildata = False
    if map_soildata:
        df_comb_clean = map_soildata_to_GPS_location(df_comb_clean.copy())
        df_comb_clean.to_csv(os.path.join(output_path, "comb_clean_soil.csv"), index=False)

    # Join df with weather data df
    weather_dt_path = os.path.join(weather_path, "era5_land_monthly_mapped.csv")
    if os.path.exists(weather_dt_path):
        df_weather = pd.read_csv(weather_dt_path)
        df_comb_clean_weather = df_comb_clean.merge(df_weather, on = 'id')
        print("Added weather data")
        df_comb_clean_weather.to_csv(os.path.join(output_path, "comb_clean_wweather.csv"), index=False)
