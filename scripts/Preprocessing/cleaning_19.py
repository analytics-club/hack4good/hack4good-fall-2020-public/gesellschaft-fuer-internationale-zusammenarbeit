import os

import pandas as pd
import numpy as np
import os

weather_path = "../../data/weather"

from scripts.Preprocessing.soil_data_mapping import map_soildata_to_GPS_location

def filter_productivities(df_comb):
    df_comb = df_comb.loc[~((df_comb["groundnuts"] == 1) &
                            ((df_comb['gr_land_prod'] > 2) | (df_comb['gr_land_prod'].isnull()))), :]

    df_comb = df_comb.loc[~((df_comb["soybeans"] == 1) &
                            ((df_comb['so_land_prod'] > 2) | (df_comb['so_land_prod'].isnull()))), :]
    return df_comb


weather_path = "../../data/weather"
def clean_19_survey(df_2019_raw, only_19=False):
    # create the new data frame
    df_2019_clean = pd.DataFrame()

    columns_to_keep = {
        "caseid": "id",
        "date": "date",
        "focusvc_1": "groundnuts",
        "focusvc_2": "soybeans",
        "radio_show": "radio_comaco",
        "fert_1": "gr_chemical",
        "man_1": "gr_manure",
        "pest_1": "gr_insecticides",
        "herb_1": "gr_herbicides",
        "fung_1": "gr_fungicides",
        "fert_2": "so_chemical",
        "man_2": "so_manure",
        "pest_2": "so_insecticides",
        "herb_2": "so_herbicides",
        "fung_2": "so_fungicides",
        # "how did you obtain the land?"
        "land_how_1": "land_how_purchased",
        "land_how_2": "land_how_rented",
        "land_how_3": "land_how_inherited",
        "land_how_4": "land_how_received",
        "land_how_5": "land_how_acquired",
        # number of fields
        "nr_field": "nr_field",
        # tech devices
        "radio_dev_1": "radio_dev_phone",
        "radio_dev_2": "radio_dev_smartphone",
        "radio_dev_3": "radio_dev_tablet",
        "radio_dev_4": "radio_dev_radio",
        "radio_dev_97": "radio_dev_other",
        "radio_dev_0": "radio_dev_none",
        # details about COMACO radio show
        "radio_time": "radio_time", # 1: weekly 2: twice/month 3: once/month 4: less 5: never
        # "with whom do you listen to COMACO radio show?"
        "radio_group_1": "radio_group_family",
        "radio_group_4": "radio_group_neighbor",
        "radio_group_2": "radio_group_producer",
        "radio_group_3": "radio_group_cooperative",
        "radio_group_97": "radio_group_other",
        # trainings
        "train_frequ": "train_frequency",
        # "what hinders you to participate more frequently?"
        "train_hinder_1": "train_hinder_1",
        "train_hinder_9": "train_hinder_9",
        "train_hinder_2": "train_hinder_2",
        "train_hinder_3": "train_hinder_3",
        "train_hinder_4": "train_hinder_4",
        "train_hinder_5": "train_hinder_5",
        "train_hinder_6": "train_hinder_6",
        "train_hinder_7": "train_hinder_7",
        "train_hinder_8": "train_hinder_8",
        "train_hinder_97": "train_hinder_97",
        # "how does your yield of groundnuts this year compare to normal?"
        "prod_comparison_1": "gr_prod_compare",
        # reasons for lower yield
        "prod_low_1_1": "gr_prod_compare_rainfall_excess",
        "prod_low_1_2": "gr_prod_compare_rainfall_shortage",
        "prod_low_1_3": "gr_prod_compare_rainfall_pattern",
        "prod_low_1_4": "gr_prod_compare_lack_fertilizer",
        "prod_low_1_5": "gr_prod_compare_lack_seeds",
        "prod_low_1_6": "gr_prod_compare_soil_degradation",
        "prod_low_1_7": "gr_prod_compare_destruction_animals",
        "prod_low_1_8": "gr_prod_compare_disease",
        "prod_low_1_9": "gr_prod_compare_labor_unavailable_family",
        "prod_low_1_10": "gr_prod_compare_labor_unavailable_hired",
        "prod_low_1_97": "gr_prod_compare_other",
        "prod_low_1_12": "gr_prod_compare_unknown",
        "prod_comparison_2": "so_prod_compare",
        "prod_low_2_1": "so_prod_compare_rainfall_excess",
        "prod_low_2_2": "so_prod_compare_rainfall_shortage",
        "prod_low_2_3": "so_prod_compare_rainfall_pattern",
        "prod_low_2_4": "so_prod_compare_lack_fertilizer",
        "prod_low_2_5": "so_prod_compare_lack_seeds",
        "prod_low_2_6": "so_prod_compare_soil_degradation",
        "prod_low_2_7": "so_prod_compare_destruction_animals",
        "prod_low_2_8": "so_prod_compare_disease",
        "prod_low_2_9": "so_prod_compare_labor_unavailable_family",
        "prod_low_2_10": "so_prod_compare_labor_unavailable_hired",
        "prod_low_2_97": "so_prod_compare_other",
        "prod_low_2_12": "so_prod_compare_unknown",
        # livestock
        "li_det_1": "livestock_goats",
        "li_det_2": "livestock_sheep",
        "li_det_3": "livestock_chicken",
        "li_det_4": "livestock_pigs",
        "li_det_5": "livestock_donkeys",
        "li_det_6": "livestock_fish",
        "li_det_7": "livestock_cattle",
        "li_det_8": "livestock_turkey",
        "li_det_9": "livestock_pigeons",
        "li_det_10": "livestock_quails",
        "li_det_11": "livestock_rabbits",
        "li_det_12": "livestock_ducks",
        "li_det_13": "livestock_guinea",
        "li_det_97": "livestock_other",
        "li_det_0": "livestock_none",
        "bee_det": "beekeeping",
        # financial services
        "fin": "financial_access",
        "fin_type_1": "financial_type_savings",
        "fin_type_2": "financial_type_bank_account",
        "fin_type_3": "financial_type_loan",
        "fin_type_4": "financial_type_insurance",
        "fin_type_97": "financial_type_other",
        "loan_am": "financial_loan_amount",
        "ins_purpose_1": "financial_insurance_crop",
        "ins_purpose_2": "financial_insurance_livestock",
        "ins_purpose_3": "financial_insurance_life",
        "ins_purpose_97": "financial_insurance_other",
        # personal questions about lack of food
        "hh_food_worry": "food_worry",
        "hh_food_nutri": "food_nutri",
        "hh_food_var": "food_variation",
        "hh_food_skip": "food_skip",
        "hh_food_less": "food_less",
        "hh_food_none": "food_none",
        "hh_food_hungry": "food_hungry",
        "hh_food_eatnone": "food_eatnone"
    }

    for col in columns_to_keep:
        df_2019_clean[columns_to_keep[col]] = df_2019_raw[col]

    def map_district(row):
        map_district = {
            "D1": "Chipata",
            "D2": "Katete",
            "D3": "Lundazi",
            "D4": "Mambwe",
            "D5": "Petauke"
        }
        return map_district[row.district]

    df_2019_clean["district"] = df_2019_raw.apply(map_district, axis=1)

    def map_village(row):
        map_village = {
            1: "Chingala",
            2: "Itaye",
            3: "Kamiya",
            4: "Mangulu 2",
            5: "Mteleza Biwi",
            6: "Ngómbe",
            7: "Bandawe",
            8: "Chaponda",
            9: "Kafipa",
            10: "Lukhalo",
            11: "Mgombela",
            12: "Zimema",
            13: "Biwi",
            14: "Chaopa",
            15: "Chazuka",
            16: "Chinyama",
            17: "Dzikolawene",
            18: "Zibwelera",
            19: "Bikoko",
            20: "Chapita",
            21: "Gaven",
            22: "Joel",
            23: "Kalomo",
            24: "Sambila",
            25: "Chimtembo",
            26: "Egichiken",
            27: "Gomani",
            28: "Julious",
            29: "Kamalaza",
            30: "Bandawe",
            31: "Chanyala",
            32: "Isaac",
            33: "Vileme",
            34: "Anderson",
            35: "Chingaipe",
            36: "Kawaza",
            37: "Nyongo",
            38: "Tazwela",
            39: "Zamanga",
            40: "Chaponda",
            41: "Chembe",
            42: "Kabamba",
            43: "Kavimba",
            44: "Malama",
            45: "Saidi",
            46: "Bikoko",
            47: "Chandema",
            48: "Dickson",
            49: "Goliat",
            50: "Mazala",
            51: "Nkhumba",
            52: "Angelo",
            53: "Chibeteka",
            54: "Dauzeni",
            55: "Kalondwe",
            56: "Lambwe",
            57: "Mando"
        }
        return map_village[row.village]

    df_2019_clean["village"] = df_2019_raw.apply(map_village, axis=1)

    def crop_rotation(row):
        crop_rotation = False
        for i in range(1, 5):
            # this year's crops are not the same as last year (on the same field i)
            rotate = (row[f'cons_crop_1_{i}'] != row[f'cons_crop_2_{i}']) or (
                    row[f'cons_crop_1_{i}'] != row[f'cons_crop_3_{i}'])
            if rotate:
                crop_rotation = rotate
        return int(crop_rotation)

    df_2019_clean["crop_rotation"] = df_2019_raw.apply(crop_rotation, axis=1)

    # land_amount in hectares
    def find_ha_value(row):
        if row.land_amount_unit == 1:  # hectares
            return row.land_ha_ha
        if row.land_amount_unit == 2:  # acres
            return row.land_ha_ac
        if row.land_amount_unit == 3:  # lima
            return row.land_ha_li

    df_2019_clean["land_amount"] = df_2019_raw.apply(find_ha_value, axis=1)

    # all agrof_default_x variables denote whether the farmer has a certain tree of type x on his fields
    # if one of these variables is equal to 1, we say that they practice agroforestry
    df_2019_clean["agroforestry"] = df_2019_raw.filter(regex="agrof_default_[1,2,3,97].*", axis=1).max(axis=1)

    # radio_dev_4 denotes the possession of a radio
    # we could also include other devices (e.g. smartphones), which technically also include a radio
    df_2019_clean["radio"] = df_2019_raw["radio_dev_4"]
    df_2019_clean["tech_devices"] = df_2019_raw.filter(regex="radio_dev_[1,2,3,4,97].*", axis=1).max(axis=1)

    # retrieve information about internet from devices
    df_2019_clean["internet"] = df_2019_raw.filter(regex="radio_dev_[1,2,3].*", axis=1).max(axis=1)

    # groundnuts
    # convert area
    # take the calculated land amount in ha for each row based on the preprocessed column
    def gr_find_ha_value(row):
        if row.area_1_un == 1:  # hectares
            return row.area_ha_ha_1
        if row.area_1_un == 2:  # acres
            return row.area_ha_ac_1
        if row.area_1_un == 3:  # lima
            return row.area_ha_li_1

    df_2019_clean["gr_area"] = df_2019_raw.apply(gr_find_ha_value, axis=1)

    # volume
    def gr_find_kg_value(row):
        if row.unit_1 == 1:  # kg
            return row.vol_1_kg
        if row.unit_1 == 2:  # bag(s) of unshelled groundnuts (50kg print)
            return row.vol_1_bag_nut50
        if row.unit_1 == 3:  #
            return row.vol_1_bag_nut25
        if row.unit_1 == 4:  #
            return row.vol_1_bag_soy50
        if row.unit_1 == 5:  #
            return row.vol_1_bag_soy25

    df_2019_clean["gr_vol"] = df_2019_raw.apply(gr_find_kg_value, axis=1)

    # household workers
    def gr_count_male(row):
        male = 0
        hours = 0
        for i in range(1, 13):
            if row[f"hh_1_gender_{i}"] == 1:
                male = male + 1
                hours = hours + row[f"hh_1_hours_{i}"]
        days = hours / 8
        return pd.Series([male, days], index=["gr_male_household", "gr_male_household_workdays"])

    def gr_count_female(row):
        female = 0
        hours = 0
        for i in range(1, 13):
            if row[f"hh_1_gender_{i}"] == 2:
                female = female + 1
                hours = hours + row[f"hh_1_hours_{i}"]
        days = hours / 8
        return pd.Series([female, days], index=["gr_female_household", "gr_female_household_workdays"])

    def gr_count_children(row):
        children = 0
        hours = 0
        for i in range(1, 13):
            if row[f"hh_1_age_{i}"] <= 18:
                children = children + 1
                hours = hours + row[f"hh_1_hours_{i}"]
        days = hours / 8
        return pd.Series([children, days], index=["gr_children_household", "gr_children_household_workdays"])

    df_2019_clean[["gr_male_household", "gr_male_household_workdays"]] = df_2019_raw.apply(gr_count_male, axis=1)
    df_2019_clean[["gr_female_household", "gr_female_household_workdays"]] = df_2019_raw.apply(gr_count_female, axis=1)
    df_2019_clean[["gr_children_household", "gr_children_household_workdays"]] = df_2019_raw.apply(gr_count_children, axis=1)


    # hired workers
    def gr_count_hired(row):
        male = 0
        male_hours = 0
        female = 0
        female_hours = 0
        children = 0
        children_hours = 0
        pay = 0
        for i in range(1, 11):
            if pd.notnull(row[f"hi_1_pay_{i}"]):
                pay = pay + row[f"hi_1_pay_{i}"]
            if row[f"hi_1_gender_{i}"] == 1:
                male = male + 1
                male_hours = male_hours + row[f"hi_1_hours_{i}"]
            if row[f"hi_1_gender_{i}"] == 2:
                female = female + 1
                female_hours = female_hours + row[f"hi_1_hours_{i}"]
            if row[f"hi_1_age_{i}"] <= 18:
                children = children + 1
                children_hours = children_hours + row[f"hi_1_hours_{i}"]
        for i in range (1,4): # assumption: group workers are male (for simplicity)
            if pd.notnull(row[f"hi_1_gr_size_{i}"]):
                pay = pay + row[f"hi_1_gr_pay_{i}"]
                male_hours = male_hours + row[f"hi_1_gr_hours_{i}"]
                male = male + row[f"hi_1_gr_size_{i}"]
        male_days = male_hours / 8
        female_days = female_hours / 8
        children_days = children_hours / 8
        return pd.Series([pay, male, male_days, female,female_days, children, children_days], index=["gr_pay_hired", "gr_male_hired", "gr_male_hired_workdays", "gr_female_hired", "gr_female_hired_workdays", "gr_children_hired", "gr_children_hired_workdays"])

    df_2019_clean[["gr_pay_hired", "gr_male_hired", "gr_male_hired_workdays", "gr_female_hired", "gr_female_hired_workdays", "gr_children_hired", "gr_children_hired_workdays"]] = df_2019_raw.apply(gr_count_hired, axis=1)

    # soy
    # convert area
    # take the calculated land amount in ha for each row based on the preprocessed column
    def so_find_ha_value(row):
        if row.area_2_un == 1:  # hectares
            return row.area_ha_ha_2
        if row.area_2_un == 2:  # acres
            return row.area_ha_ac_2
        if row.area_2_un == 3:  # lima
            return row.area_ha_li_2

    df_2019_clean["so_area"] = df_2019_raw.apply(so_find_ha_value, axis=1)

    # volume
    def so_find_kg_value(row):
        if row.unit_2 == 1:  # kg
            return row.vol_2_kg
        if row.unit_2 == 2:  # bag(s) of unshelled groundnuts (50kg print)
            return row.vol_2_bag_nut50
        if row.unit_2 == 3:  #
            return row.vol_2_bag_nut25
        if row.unit_2 == 4:  #
            return row.vol_2_bag_soy50
        if row.unit_2 == 5:  #
            return row.vol_2_bag_soy25

    df_2019_clean["so_vol"] = df_2019_raw.apply(so_find_kg_value, axis=1)

    # household workers
    def so_count_male(row):
        male = 0
        hours = 0
        for i in range(1, 10):
            if row[f"hh_2_gender_{i}"] == 1:
                male = male + 1
                hours = hours + row[f"hh_2_hours_{i}"]
        days = hours / 8
        return pd.Series([male, days], index=["so_male_household", "so_male_household_workdays"])

    def so_count_female(row):
        female = 0
        hours = 0
        for i in range(1, 10):
            if row[f"hh_2_gender_{i}"] == 1:
                female = female + 1
                hours = hours + row[f"hh_2_hours_{i}"]
        days = hours / 8
        return pd.Series([female, days], index=["so_female_household", "so_female_household_workdays"])

    def so_count_children(row):
        children = 0
        hours = 0
        for i in range(1, 10):
            if row[f"hh_1_age_{i}"] <= 18:
                children = children + 1
                hours = hours + row[f"hh_1_hours_{i}"]
        days = hours / 8
        return pd.Series([children, days], index=["so_children_household", "so_children_household_workdays"])

    df_2019_clean[["so_male_household", "so_male_household_workdays"]] = df_2019_raw.apply(so_count_male, axis=1)
    df_2019_clean[["so_female_household", "so_female_household_workdays"]] = df_2019_raw.apply(so_count_female, axis=1)
    df_2019_clean[["so_children_household", "so_children_household_workdays"]] = df_2019_raw.apply(so_count_children, axis=1)


    # hired workers
    def so_count_hired(row):
        male = 0
        male_hours = 0
        female = 0
        female_hours = 0
        children = 0
        children_hours = 0
        pay = 0
        for i in range(1, 16):
            if pd.notnull(row[f"hi_2_pay_{i}"]):
                pay = pay + row[f"hi_2_pay_{i}"]
            if row[f"hi_2_gender_{i}"] == 1:
                male = male + 1
                male_hours = male_hours + row[f"hi_2_hours_{i}"]
            if row[f"hi_2_gender_{i}"] == 2:
                female = female + 1
                female_hours = female_hours + row[f"hi_2_hours_{i}"]
            if row[f"hi_2_age_{i}"] <= 18:
                children = children + 1
                children_hours = children_hours + row[f"hi_2_hours_{i}"]
        for i in range (1,6): # assumption: group workers are male (for simplicity)
            if pd.notnull(row[f"hi_2_gr_size_{i}"]):
                pay = pay + row[f"hi_2_gr_pay_{i}"]
                male_hours = male_hours + row[f"hi_2_gr_hours_{i}"]
                male = male + row[f"hi_2_gr_size_{i}"]
        male_days = male_hours / 8
        female_days = female_hours / 8
        children_days = children_hours / 8
        return pd.Series([pay, male, male_days, female, female_days, children, children_days], index=["so_pay_hired", "so_male_hired", "so_male_hired_workdays", "so_female_hired", "so_female_hired_workdays", "so_children_hired", "so_children_hired_workdays"])

    df_2019_clean[["so_pay_hired", "so_male_hired", "so_male_hired_workdays", "so_female_hired", "so_female_hired_workdays", "so_children_hired", "so_children_hired_workdays"]] = df_2019_raw.apply(so_count_hired, axis=1)

    # "Topics you would like to learn more about" -> measure for thirst for knowledge
    def ideas(row):
        curiosity = 0
        if pd.notnull(row.idea_1):
            curiosity = curiosity + 1
        if pd.notnull(row.idea_2):
            curiosity = curiosity + 1
        if pd.notnull(row.idea_3):
            curiosity = curiosity + 1
        return curiosity

    df_2019_clean["curiosity"] = df_2019_raw.apply(ideas, axis=1)

    # Production of different crops (number of different crops)
    df_2019_clean["production_other_crops"] = df_2019_raw["crop_prod"].apply(lambda row: len(row.split()))

    # fill missing columns with NaN
    impute_cols = ["so_chemical_amount", "gr_manure_amount", "so_manure_amount",
                   "gr_fungicides_amount", "so_insecticides_amount", "gr_insecticides_amount", "so_fungicides_amount",
                   "so_herbicides_amount", "gr_herbicides_amount", "gr_chemical_amount"]
    for col in impute_cols:
        df_2019_clean[col] = np.nan
    # fill NaN with zeros for binary variables (this makes sense, e.g. for questions that are only asked if a certain condition is fulfilled
    binary_fill_values = {
        "gr_chemical": 0,
        "gr_manure": 0,
        "gr_insecticides": 0,
        "gr_herbicides": 0,
        "gr_fungicides": 0,
        "so_chemical": 0,
        "so_manure": 0,
        "so_insecticides": 0,
        "so_herbicides": 0,
        "so_fungicides": 0,
        "radio_group_family": 0,
        "radio_group_neighbor": 0,
        "radio_group_producer": 0,
        "radio_group_cooperative": 0,
        "radio_group_other": 0,
        "radio_time": 5,
        "train_hinder_1": 0,
        "train_hinder_9": 0,
        "train_hinder_2": 0,
        "train_hinder_3": 0,
        "train_hinder_4": 0,
        "train_hinder_5": 0,
        "train_hinder_6": 0,
        "train_hinder_7": 0,
        "train_hinder_8": 0,
        "train_hinder_97": 0,
        "gr_prod_compare_rainfall_excess": 0,
        "gr_prod_compare_rainfall_shortage": 0,
        "gr_prod_compare_rainfall_pattern": 0,
        "gr_prod_compare_lack_fertilizer": 0,
        "gr_prod_compare_lack_seeds": 0,
        "gr_prod_compare_soil_degradation": 0,
        "gr_prod_compare_destruction_animals": 0,
        "gr_prod_compare_disease": 0,
        "gr_prod_compare_labor_unavailable_family": 0,
        "gr_prod_compare_labor_unavailable_hired": 0,
        "gr_prod_compare_other": 0,
        "gr_prod_compare_unknown": 0,
        "so_prod_compare_rainfall_excess": 0,
        "so_prod_compare_rainfall_shortage": 0,
        "so_prod_compare_rainfall_pattern": 0,
        "so_prod_compare_lack_fertilizer": 0,
        "so_prod_compare_lack_seeds": 0,
        "so_prod_compare_soil_degradation": 0,
        "so_prod_compare_destruction_animals": 0,
        "so_prod_compare_disease": 0,
        "so_prod_compare_labor_unavailable_family": 0,
        "so_prod_compare_labor_unavailable_hired": 0,
        "so_prod_compare_other": 0,
        "so_prod_compare_unknown": 0,
        "livestock_goats": 0,
        "livestock_sheep": 0,
        "livestock_chicken": 0,
        "livestock_pigs": 0,
        "livestock_donkeys": 0,
        "livestock_fish": 0,
        "livestock_cattle": 0,
        "livestock_turkey": 0,
        "livestock_pigeons": 0,
        "livestock_quails": 0,
        "livestock_rabbits": 0,
        "livestock_ducks": 0,
        "livestock_guinea": 0,
        "livestock_other": 0,
        "livestock_none": 0,
        "financial_type_savings": 0,
        "financial_type_bank_account": 0,
        "financial_type_loan": 0,
        "financial_type_insurance": 0,
        "financial_type_other": 0,
        "financial_loan_amount": 0,
        "financial_insurance_crop": 0,
        "financial_insurance_livestock": 0,
        "financial_insurance_life": 0,
        "financial_insurance_other": 0
    }

    # compute land productivity
    df_2019_clean["gr_land_prod"] = df_2019_clean.gr_vol / (df_2019_clean.gr_area * 1000)
    df_2019_clean["so_land_prod"] = df_2019_clean.so_vol / (df_2019_clean.so_area * 1000)
    df_2019_clean.fillna(value=binary_fill_values, inplace=True)

    # Village to GPS mapping

    # Read in the file with all available villages ("AV-file")
    df_villages = pd.read_excel("../../data/GIZ_input_files/data_for_cleaning/VILLAGE LOCATION COMACO AREAS-East(1).xlsx")

    # Define set of all available villages (with only lower chars and whitespaces removed)
    available_villages = set(df_villages["village"].str.lower().replace(regex=r"\s+", value="").values)
    available_villages -= {"n", "m", "\\n", "\n", "", np.NaN}

    # Define set of all available camps (with only lower chars and whitespaces removed)
    available_camps = set(df_villages["Camp/Vag"].str.lower().replace(regex=r"\s+", value="").values)
    available_camps -= {"n", "m", "\\n", "\n", "", np.NaN}

    # Define mappings from villages in AV-file to their gps attributes (could in principle also be done with a JOIN)
    dict_latitudes = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["latitude"]))
    dict_longitudes = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["longitude"]))
    dict_altitudes = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["elevation"]))
    dict_accuracy = dict(
        zip(df_villages["village"].str.lower().replace(regex=r"\s+", value=""), df_villages["accuracy"]))

    del dict_latitudes[np.NaN], dict_longitudes[np.NaN], dict_altitudes[np.NaN], dict_accuracy[np.NaN]

    # Apply mapping from 2019 file to coordinates from AV-file
    df_2019_raw["village_name_processed"] = df_2019_clean["village"].str.lower().replace(regex=r"\s+", value="")

    village_mapping = dict()

    for village_name in set(df_2019_raw["village_name_processed"]):
        if village_name in available_villages:
            village_mapping[village_name] = village_name

    df_2019_raw["village_name_available"] = df_2019_raw["village_name_processed"].map(village_mapping)
    df_2019_raw["gps__latitude"] = df_2019_raw["village_name_available"].map(dict_latitudes)
    df_2019_raw["gps__longitude"] = df_2019_raw["village_name_available"].map(dict_longitudes)
    df_2019_raw["gps__altitude"] = df_2019_raw["village_name_available"].map(dict_altitudes)

    df_2019_clean["gps_lat"] = df_2019_raw["gps__latitude"]
    df_2019_clean["gps_lon"] = df_2019_raw["gps__longitude"]
    df_2019_clean["gps_alt"] = df_2019_raw["gps__altitude"]
    df_2019_clean.loc[df_2019_clean["gps_alt"] >= 810, "plateau_or_valley"] = "plateau"
    df_2019_clean.loc[df_2019_clean["gps_alt"] < 810, "plateau_or_valley"] = "valley"

    # columns to keep
    df_final_variables = pd.read_csv("../../data/GIZ_input_files/data_for_cleaning/Questionaire - Manual Inspection of Questions - Final variable mapping.csv")
    final_cols = df_final_variables["Final column name"].tolist()
    final_cols.append("gps_alt")

    # return dataframe for analyzing 2019 only (1) or merging with other years
    if only_19:
        return df_2019_clean.drop(columns=impute_cols)
    else:
        return df_2019_clean[final_cols]

if __name__ == "__main__":
    # read in the raw data
    df_2019_raw = pd.read_csv("../../data/GIZ_input_files/answers/GIC Zambia_Data_HH_EP_ 2019.csv", low_memory=False)
    # write the cleaned data frame into a csv
    df_2019_clean = clean_19_survey(df_2019_raw, only_19=True)
    df_2019_clean = df_2019_clean.dropna(axis=0, subset=["gps_lat", "gps_lon"], how="any").reset_index(drop=True)  # Drop all rows which do not have GPS data
    df_2019_clean.to_csv("../../data/clean_survey_data/2019_clean.csv", index=False)

    # Join df with soil data if wanted
    map_soildata=True
    if map_soildata:
        df_2019_clean = map_soildata_to_GPS_location(df_2019_clean.copy())
        df_2019_clean.to_csv("../../data/clean_survey_data/2019_clean_soil.csv", index=False)

    # Join df with weather data df
    weather_dt_path = os.path.join(weather_path, "era5_land_monthly_mapped.csv")
    if os.path.exists(weather_dt_path):
        df_weather = pd.read_csv(weather_dt_path)
        df_2019_clean_weather = df_2019_clean.merge(df_weather, on = 'id')
        print("Added weather data")
        df_2019_clean_weather.to_csv("../../data/clean_survey_data/2019_clean_soil_weather.csv", index=False)
