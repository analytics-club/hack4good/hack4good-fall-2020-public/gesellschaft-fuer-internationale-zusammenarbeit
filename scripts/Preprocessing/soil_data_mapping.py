from owslib.wcs import WebCoverageService
import time
import rasterio
import os
import pandas as pd
import numpy as np

def download_all_soil_maps(lon_min=30, lon_max=34, lat_min=-15, lat_max=-10, output_path = "../../data/soildata"):
    '''
    Download all required soil maps. Currently only the mean value for each variable is considered.

    The latitude and longitude parameters specify the box in which the data should be downloaded and are pre-assigned to values over Eastern Zambia.

    Output path specifies where to save the variable files.
    '''
    soil_features = ["bdod",  # Bulk density of the fine earth fraction 	cg/cm³ 	100 	kg/dm³
                     "cec",  # Cation Exchange Capacity of the soil 	mmol(c)/kg 	10 	cmol(c)/kg
                     "cfvo",  # Volumetric fraction of coarse fragments (> 2 mm) 	cm3/dm3 (vol‰) 	10 	cm3/100cm3 (vol%)
                     "clay", # Proportion of clay particles (< 0.002 mm) in the fine earth fraction 	g/kg 	10 	g/100g (%)
                     "nitrogen",  # Total nitrogen (N) 	cg/kg 	100 	g/kg
                     "phh2o",  # Soil pH 	pHx10 	10 	pH
                     "sand", # Proportion of sand particles (> 0.05 mm) in the fine earth fraction 	g/kg 	10 	g/100g (%)
                     "silt", # Proportion of silt particles (≥ 0.002 mm and ≤ 0.05 mm) in the fine earth fraction 	g/kg 	10 	g/100g (%)
                     "soc",  # Soil organic carbon content in the fine earth fraction 	dg/kg 	10 	g/kg
                     "ocd",  # Organic carbon density 	hg/dm³ 	10 	kg/dm³
                     "ocs"]  # Organic carbon stocks 	t/ha 	10 	kg/m² ; Only has 0-30 depth

    for feature in soil_features:
        print(f"Retrieving {feature}")
        wcs = WebCoverageService(f'http://maps.isric.org/mapserv?map=/map/{feature}.map', version='2.0.1')
        mean_coverage_ids = [content for content in wcs.contents.keys() if "mean" in content] # To download only mean

        # Loop over all depths
        for cov_id in mean_coverage_ids:
            response = wcs.getCoverage(
                identifier=[cov_id],
                outputcrs="http://www.opengis.net/def/crs/EPSG/0/4326",
                subsets=[('X', lon_min, lon_max), ('Y', lat_min, lat_max)],
                Subsettingcrs="http://www.opengis.net/def/crs/EPSG/0/4326",
                resx=250, resy=250,
                format='image/tiff'
            )

            # Save each variable at each depth to its own TIF file. Make sure the path exists.
            with open(os.path.join(output_path, f"{cov_id}.tif"), 'wb') as file:
                file.write(response.read())
        time.sleep(3)

def map_soildata_to_GPS_location(df, input_path = "../../data/soildata"):
    '''
    Calculate the rectangle of distance*250m around the GPS location of each survey.
    Calculate the mean of all soil data variables in in this frame.
    Return the dataframe now enriched by the soil data.

    Input: Dataframe with no missing GPS coordinates. Input path of soil variables
    Output: Concatenated dataframe with original and soil data
    '''
    km = 3 # distance in km away from GPS location
    distance = km*4 # Amount of datapoints in each direction

    # Get all filenames of before downloaded soildata files and remove non-soil files
    soildata_filenames = os.listdir(input_path)
    if ".gitignore" in soildata_filenames:
        soildata_filenames.remove(".gitignore")
    if ".DS_Store" in soildata_filenames:
        soildata_filenames.remove(".DS_Store")

    soilfeatures_dict = {} # Dict for columns

    # Loop over all downloaded files and open them only once
    for filename in soildata_filenames:
        print(filename)
        with rasterio.open(os.path.join(input_path, filename), driver="GTiff") as variable:
            variable_numpygrid = variable.read(1) # Get first band (there is only one)
            mean_list = [] # List for the rows of a later soil column

            # Loop over all rows in the dataframe
            for df_index, df_row in df.iterrows():
                # Get index of given GPS coordinate
                row_index, col_index = rasterio.transform.TransformMethodsMixin.index(variable, x=df_row["gps_lon"], y=df_row["gps_lat"])
                # Calculate mean over rectangular window around GPS coordinate
                variable_mean = variable_numpygrid[(row_index-distance):(row_index+distance+1), (col_index-distance):(col_index+distance+1)].mean()
                mean_list.append(variable_mean)

            # Store column in a dict
            soilfeatures_dict.update({filename[:-4]: mean_list})

    return pd.concat([df, pd.DataFrame(soilfeatures_dict)], axis=1)

def prepare_all_soil_files_in_folder_around_villages_for_tableau(df, km=3, input_path = "../../data/soildata", output_path= ""):
    '''
        Retrieve the rectangle of set kilometers around the GPS location of each survey.
        Return the dataframe in tableau long table format.

        Input: Dataframe with no missing GPS coordinates
        Output: Tableau long table CSV
        '''
    distance = km * 4  # Amount of datapoints in each direction (due to 250m resolution)

    # Get all filenames of before downloaded soildata files and remove non-soil files
    soildata_filenames = os.listdir(input_path)
    if ".gitignore" in soildata_filenames:
        soildata_filenames.remove(".gitignore")
    if ".DS_Store" in soildata_filenames:
        soildata_filenames.remove(".DS_Store")

    # Generate feature which distinguishes between plateau and valley
    df["plateau_or_valley"] = np.NaN
    df.loc[df["gps_alt"] >= 810, "plateau_or_valley"] = "plateau"
    df.loc[df["gps_alt"] < 810, "plateau_or_valley"] = "valley"

    soil_data_rows = []
    # Loop over all downloaded files and open them only once
    for filename in soildata_filenames:
        print(filename[:-4])
        with rasterio.open(os.path.join(input_path, filename), driver="GTiff") as variable:

            variable_numpygrid = variable.read(1)  # Get first band (there is only one)

            # Loop over all rows in the dataframe
            for df_index, df_row in df.drop_duplicates(subset=["gps_lon", "gps_lat"]).iterrows():
                # Get index of given GPS coordinate
                row_index, col_index = rasterio.transform.TransformMethodsMixin.index(variable, x=df_row["gps_lon"],
                                                                                      y=df_row["gps_lat"])
                # Get all values in rectangular around GPS coordinate
                for i in range(-distance, distance + 1, 1):
                    for j in range(-distance, distance + 1, 1):

                        lon, lat = variable.xy(row_index + i, col_index + j)
                        variable_value = variable_numpygrid[row_index + i, col_index + j]

                        soil_data_rows.append(
                            [lon, lat, filename[:-4], variable_value, df_row["gps_alt"], df_row["plateau_or_valley"]])

    df_soil = pd.DataFrame(soil_data_rows,
                           columns=["gps_lon", "gps_lat", "variable", "value", "gps_alt", "plateau_or_valley"])

    # Drop values which are retrieved more than once
    df_soil_reduced = df_soil.drop_duplicates(subset=["gps_lon", "gps_lat", "variable"])

    # Drop rows which have a zero (NaN) in their value
    df_soil_reduced_filter = df_soil_reduced[(df_soil_reduced != 0).all(axis=1)]

    # Save file to output path
    df_soil_reduced_filter.to_csv(os.path.join(output_path, "soil_data_around_villages.csv"), index=False)

    return df_soil_reduced_filter # If needed

if __name__ =="__main__":
    # Download all soil features if necessary
    soil_data_downloaded = True
    if not soil_data_downloaded:
        download_all_soil_maps()

    # Here for the combined dataframe, also works with other dataframes which offer latitude, longitude and altitude
    df = pd.read_csv("../../data/clean_survey_data/comb_clean.csv")
    df = df.dropna(axis=0, subset=["gps_lat", "gps_lon"], how="any").reset_index(drop=True)
    df = map_soildata_to_GPS_location(df)
    df.to_csv("../../data/clean_survey_data/df_comb_soil.csv")

    # Map to tableau long table format
    transform_to_tableau = True
    if transform_to_tableau:
        prepare_all_soil_files_in_folder_around_villages_for_tableau(df, km=1, input_path="../../data/soildata", output_path="../../data/tableau_format")