import xarray as xr
import os

"""
First download elevation data from wordclim (only has to be done once):
go to elevation directory (data/elevation) and run:

wget https://biogeo.ucdavis.edu/data/worldclim/v2.1/base/wc2.1_30s_elev.zip
unzip wc2.1_30s_elev.zip

(or download and extract manually)
"""
area_of_interest = [-9, 22, -18, 34]
elev_dir = "../../data/elevation/"


def main():
    """Converts and saves elevation from tif to netcdf file"""
    elev_da = xr.open_rasterio(os.path.join(elev_dir, "wc2.1_30s_elev.tif"))
    elev_da = elev_da.drop("band").rename({"x": "lon", "y": "lat"})

    elev_da = elev_da.loc[dict(lon=slice(area_of_interest[1], area_of_interest[3]), \
        lat=slice(area_of_interest[0], area_of_interest[2]))]

    elev_da.to_netcdf(os.path.join(elev_dir, "elevation_data.nc"))


if __name__ == "__main__":
    main()
