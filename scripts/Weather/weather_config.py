#!/usr/bin/env python

import os
import datetime

"""

Weather variables:

sos (start of rain season)
- sos: start of season (day of the year)
- sos_diff: difference in start of season to long term average
- sos_avg: long-term average of start of season

stp (season total percipitation)
- stp: season total percipitation [metres]
- stp_rel: relative stp compared to long-term avg
- stp_diff: difference in stp compared to long-term avg

fld (flash flood):
- fld_events: number of flood events (rainfall above certain threshold) for the given growing season
- fld_avg: n of fld_events over long term average

Other monthly weather variables: {var name}_{month of rainy season}
- example: t2m_0 (temperature at 2m) in 1st month of rainy season (October, or check earliest_strt)


short_var                                          full_name
0     lai_lv                    leaf_area_index,_low_vegetation
1         ro                                             runoff
2      evavt          evaporation_from_vegetation_transpiration
3        skt                                   skin_temperature
4       ssro                                 sub-surface_runoff
5        str                      surface_net_thermal_radiation
6       strd                surface_thermal_radiation_downwards
7        t2m                                2_metre_temperature
8      evabs                         evaporation_from_bare_soil
9       stl2                           soil_temperature_level_2
10      sshf                         surface_sensible_heat_flux
11       pev                              potential_evaporation
12       sro                                     surface_runoff
13      stl3                           soil_temperature_level_3
14       d2m                       2_metre_dewpoint_temperature
15     swvl4                      volumetric_soil_water_layer_4
16     evaow  evaporation_from_open_water_surfaces_excluding...
17     swvl2                      volumetric_soil_water_layer_2
18    lai_hv                   leaf_area_index,_high_vegetation
19     swvl3                      volumetric_soil_water_layer_3
20      stl1                           soil_temperature_level_1
21       fal                                    forecast_albedo
22       ssr                        surface_net_solar_radiation
23        tp                                total_precipitation
24     swvl1                      volumetric_soil_water_layer_1
25      stl4                           soil_temperature_level_4
26        es                                   snow_evaporation
27         e                                        evaporation
28     evatc                 evaporation_from_the_top_of_canopy
29       src                             skin_reservoir_content
30      ssrd                  surface_solar_radiation_downwards
31      slhf                           surface_latent_heat_flux

"""


# paths
weather_path = "../../data/weather"
monthly_path = os.path.join(weather_path, "era_monthly")
hourly_path = os.path.join(weather_path, "era_hourly")
survey_path = "../../data/clean_survey_data/comb_clean.csv"

## Area of interest (download weather data in these coordinates)
area_of_interest = [-9, 22, -18, 34]

## Years of interest
y_strt = 1982 # earliest available year for era5 data 1982
y_end = datetime.datetime.now().year

# Variables to download monthly data for:
monthly_variables = [
    # change here to add further variables to download monthly data for
    '2m_dewpoint_temperature', '2m_temperature', 'evaporation_from_bare_soil',
    'evaporation_from_open_water_surfaces_excluding_oceans', 'evaporation_from_the_top_of_canopy', 
    'evaporation_from_vegetation_transpiration',
    'forecast_albedo', 'leaf_area_index_high_vegetation', 'leaf_area_index_low_vegetation',
    'potential_evaporation', 'runoff', 'skin_reservoir_content',
    'skin_temperature', 'snow_evaporation', 'soil_temperature_level_1',
    'soil_temperature_level_2', 'soil_temperature_level_3', 'soil_temperature_level_4',
    'sub_surface_runoff', 'surface_latent_heat_flux', 'surface_net_solar_radiation',
    'surface_net_thermal_radiation', 'surface_runoff', 'surface_sensible_heat_flux',
    'surface_solar_radiation_downwards', 'surface_thermal_radiation_downwards', 'total_evaporation',
    'total_precipitation', 'volumetric_soil_water_layer_1', 'volumetric_soil_water_layer_2',
    'volumetric_soil_water_layer_3', 'volumetric_soil_water_layer_4'
]

# Variables to download hourly data for:
hourly_variables = ["total_precipitation", "2m_temperature"]

# Number of threads to use when downloading
threads = 12

## Configuration for weather stats calculations
# year range to calculate rainfall statistics
years = list(range(y_strt+1, y_end+1)) # year range

# setup for start of season (SOS) calculation
max_season_length = 6 # in months
earliest_strt = 10  # earliest start of season (10->October)
latest_strt = earliest_strt + 4 # latest start of season
first_dek_thr = 0.025 # rainfall thresholds for 1st dekade (10 days),
sec_thrd_dek_thr = 0.020 # and 2nd+3rd dekade

# threshold for flood/extreme rainfall event (24h percipitation threshold)
flood_thr = 0.05 # in metres 

## For mapping weather data to survey locations
# years in survey data with location information
years_to_map = [2016, 2018, 2019]
# threshold for 
distance_threshold = 20 # In Kilometres

# coordinate margin for tableau style long table generation
coord_margin = 0.5