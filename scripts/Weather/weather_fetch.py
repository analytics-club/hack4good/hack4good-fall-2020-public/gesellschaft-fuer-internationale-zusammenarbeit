#!/usr/bin/env python

import cdsapi
import xarray as xr
import os
import pandas as pd
import datetime
from pathos.threading import ThreadPool as Pool

import weather_config as config


def download_weather_data(path, setup, dataset):
    client = cdsapi.Client() #connect to the server
    try:
        client.retrieve(dataset, setup, path)
    except:
        print("Data not available")
        #print(setup["year"] + "-" + setup["month"])


def download_monthly_weather(variables, year, month, path):
    data_set = 'reanalysis-era5-land-monthly-means'

    set_up = {
        'format': 'netcdf',
        'variable': variables,
        'time': "00:00", 
        'year': year,
        'month': month,
        'product_type': 'monthly_averaged_reanalysis',
        'area': config.area_of_interest,
    }
    download_weather_data(path, set_up, data_set)


def get_newest_m_data(path):
    """Downloads latest monthly weather available"""
    files = os.listdir(path) # check what data is already present in path dir
    srt = datetime.datetime(config.y_strt, 1, 1)
    now = datetime.datetime.today()

    missing = [date for date in pd.date_range(srt, now, freq='MS') if date.strftime(f"era5_m_%Y_%m.nc") not in files]

    for m in missing:
        download_monthly_weather(config.monthly_variables, m.year, m.month, \
            os.path.join(path, f"era5_m_{m.year}_{m.month}.nc"))


def fetch_hourly_data(variables, year, months, output_dir, threads = 12):
    """ Fetch hourly data and saves to monthly files """
    data_set = 'reanalysis-era5-land'

    set_up = {
        'format': 'netcdf',
        'variable': variables,
        'day': ["%02d" % (i,) for i in range(1, 32)],
        'time': "00:00", # just take 00:00 value as day aggregate
        'area': config.area_of_interest,
    }

    def get_month_data(y, m):
        setup = dict(set_up, month = m, year = y)
        file_path = os.path.join(output_dir, f"era5_h_{y}_{m}.nc")
        download_weather_data(file_path, setup, data_set)

    with Pool(threads) as p:
        p.map(get_month_data, len(months)*[str(year)], ["%02d" % (i,) for i in months])


def get_newest_h_data(path, threads):
    """Downloads latest weather available"""
    files = os.listdir(path) # check what data is already present in path dir
    srt = datetime.datetime(y_strt, 1, 1) 
    now = datetime.datetime.today()

    missing = [date for date in pd.date_range(srt, now, freq='MS') if date.strftime(f"era5_h_%Y_%m.nc") not in files]
    missing_df = pd.DataFrame([{"month": x.month, "year": x.year} for x in missing])

    print(f"""Trying to load data for {[x.strftime("Month: %m Year: %Y") for x in missing]}""")
    
    for y in missing_df.year.unique():
        fetch_hourly_data(config.hourly_variables, y, missing_df[missing_df.year == y].month, path, threads = threads)


def main(): 
    get_newest_h_data(config.hourly_path, config.threads)
    get_newest_m_data(config.monthly_path)


if __name__ == "__main__":
    main()