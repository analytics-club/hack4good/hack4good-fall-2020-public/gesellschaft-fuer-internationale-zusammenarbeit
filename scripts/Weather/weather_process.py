#!/usr/bin/env python

import os
import numpy as np
import xarray as xr
import pandas as pd
from tqdm import tqdm
import datetime
from dateutil.relativedelta import relativedelta

import weather_config as config


def find_sos(dekade_sum_ar, start_day, latest_strt, first_dek_th, sec_th_dek_th):
    """ 
    Returns array with SOS (day-of-year) 
    """
    rain_start = np.zeros(shape = dekade_sum_ar.shape[1:3])

    for i in range(len(dekade_sum_ar) - 19):
        # conditions for start of season (for first dekad, and second and third dekad)
        first_dek_cond = (dekade_sum_ar[i] >= first_dek_th)
        sec_third_dek_cond = ((dekade_sum_ar[i+9] + dekade_sum_ar[i+19]) >= sec_th_dek_th)
        
        already_rain = (rain_start == False) # ignore points that have already started
        strt = np.logical_and(first_dek_cond, sec_third_dek_cond) # points with starting conditions fulfilled
        rain_start = np.add(rain_start, np.multiply(already_rain, strt) * (i + start_day)) # assign day in year where season has started at this time step
        #break
    return rain_start


def comp_rain_measures(y_strt, y_end, path):
    """
    Return rain season variables from era5land tp dataset
    - SOS: Start Of (rainy) Season
    - STP: Total rainfall in season (October - April)
    - fld: Flood events (24h percipitation over threshold)

    Example: 2017/2018 rainy season -> 2018 rainy season
    """
    years = list(range(y_strt+1, y_end+1)) # cannot get earliest year since rain season starts in prev year

    print("Computing rain measures...")

    for i, year in enumerate(tqdm(years)):
        season_months = [(datetime.datetime(year-1, config.earliest_strt, 1) + relativedelta(months=+m)).strftime("%Y_%m") 
                        for m in range(config.max_season_length)]
        file_paths = [os.path.join(path, f"era5_h_{m}.nc") for m in season_months]
        season_dt = xr.merge([xr.load_dataset(f) for f in file_paths]).sel(time = datetime.time(0)) # select only entry for 00:00

        if i == 0:
            _, lon, lat = season_dt.indexes.values()
            # output arrays: start-of-season (SOS), total rainfall in season (seas_tot), flood (extreme rainfall)
            sos_arr = np.empty(shape = (len(years), len(lat), len(lon)))
            seas_tot_arr = np.empty(shape = (len(years),  len(lat), len(lon)))
            fld_arr = np.empty(shape = (len(years), len(lat), len(lon)))

        #by_day = by_day.assign(dayofyear = lambda x: x.time.dt.dayofyear)
        dakade_sums = season_dt.rolling(time = 10).sum(dim="tp").dropna("time")
        start_day = season_dt.time.dt.dayofyear.values[0]
        # get start of season
        sos_arr[i] = find_sos(dakade_sums.tp.values, start_day, config.latest_strt, config.first_dek_thr, config.sec_thrd_dek_thr)
        # get total season rainfall
        seas_tot_arr[i] = season_dt.sum(dim = "time").tp.values
        # get number of extreme rain fall events (floods), days with rainfall above threshold
        fld_arr[i] = (season_dt.tp >= config.flood_thr).sum(dim="time").values
    
    # create new data arrays
    da_sos = xr.DataArray(sos_arr, coords = {"time": (["time"], years), 'latitude': (['latitude'], lat), 'longitude': (['longitude'], lon)}, dims = ["time", "latitude", "longitude"])
    da_stp = xr.DataArray(seas_tot_arr, coords = {"time": (["time"], years), 'latitude': (['latitude'], lat), 'longitude': (['longitude'], lon)}, dims = ["time", "latitude", "longitude"])
    da_fld = xr.DataArray(fld_arr, coords = {"time": (["time"], years), 'latitude': (['latitude'], lat), 'longitude': (['longitude'], lon)}, dims = ["time", "latitude", "longitude"])

    # average values over whole time range
    da_sos_avg = da_sos.mean(dim = "time")
    da_stp_avg = da_stp.mean(dim = "time")
    da_fld_average = da_fld.mean(dim = "time")
    avg_ds = xr.Dataset({"sos_avg": da_sos_avg, "stp_avg": da_stp_avg, "fld_avg": da_fld_average})

    # further derived metrics
    sos_diff = da_sos - da_sos_avg
    stp_diff = da_stp - da_stp_avg
    stp_rel = da_stp / da_stp_avg

    out_ds = xr.Dataset({
        "sos": da_sos, "sos_diff": sos_diff, 
        "stp": da_stp, "stp_diff": stp_diff, "stp_rel": stp_rel,
        "fld_events": da_fld})
    return (out_ds, avg_ds)


def map_weather_to_locs(survey_data, years_to_map, monthly_era, rain_era, rain_avg_era):
    """ 
    Maps era5 land data to gps locations in survey data 
    """
    comb_rows = []

    for year in years_to_map:
        print(f"Mapping for {year} survey...")
        y_dt = survey_data[survey_data.year == year]
        ids = y_dt.id.values
        coords = y_dt[["gps_lat", "gps_lon"]].values
        
        season_slice = slice(f"{year-1}-10-01", f"{year}-4-01") # relevant timeframe
        monthly_season = monthly_era.sel(time = season_slice) # all relevant variables from era5
        yearly_rain = rain_era.sel(time = year) # sos,..
        
        for s_id, coord in zip(tqdm(ids), coords):
            row_res = {"id": s_id}

            # (1) monthly era weather data 0: October - 6: April
            for var in list(monthly_season.keys()):
                monthly_var = monthly_season[var]
                for t in range(len(list(monthly_var.time))):
                    row_res[f"{var}_{t}"] =  monthly_var[t].sel(latitude = coord[0], longitude = coord[1],
                                                                method = "nearest").values
            # (2) yearly rainfall data (SOS, total percipitation, flood events)
            for var in list(yearly_rain.keys()):
                row_res[f"{var}"] =  yearly_rain[var].sel(latitude = coord[0], longitude = coord[1], method = "nearest").values
            # (3) long-term average rainfall data (SOS, total percipitation, flood events)
            for var in list(rain_avg_era.keys()):
                row_res[f"{var}"] =  rain_avg_era[var].sel(latitude = coord[0], longitude = coord[1], method = "nearest").values
                
            comb_rows.append(row_res)

    return pd.DataFrame(comb_rows)


def export_long_table(mask_survey_areas = True):
    """
    Generates weather data of survey areas in tableau long table format
    """
    print("Load datasets...")
    survey_data = pd.read_csv(config.survey_path)
    out_ds = xr.load_dataset(os.path.join(config.weather_path, "rain_season_dt.nc"))
    avg_ds = xr.load_dataset(os.path.join(config.weather_path, "rain_season_avg_dt.nc"))
    monthly_era = xr.open_mfdataset(paths=os.path.join(config.monthly_path, "*.nc"))

    _, lats, lons = out_ds.indexes.values()

    if mask_survey_areas: # make mask to get datapoints around surveys
        print("Make mask for survey areas...")
        coords = survey_data[["gps_lat", "gps_lon"]].to_numpy()
        coords = np.unique(coords.round(2), axis = 0)
        mask = xr.DataArray(np.ones(shape = (len(lats), len(lons))), coords = {'lat': (['lat'], lats), 'lon': (['lon'], lons)}, 
                                   dims = ["lat", "lon"])
        survey_mask = np.zeros(shape = (len(lats), len(lons)))
        coord_margin = config.coord_margin

        for lat, lon in tqdm(coords):
            lat_min, lat_max = (lat - coord_margin, lat + coord_margin)
            lon_min, lon_max = (lon - coord_margin, lon + coord_margin)
            survey_mask = survey_mask + mask.where(((lat_min<mask.lat) & (mask.lat<lat_max)), other=0).where((lon_min<mask.lon) & (mask.lon<lon_max), other=0)

        survey_mask = survey_mask.values
        survey_mask[survey_mask>0] = 1
        smask = xr.DataArray(survey_mask, coords = {'lat': (['lat'], lats), 'lon': (['lon'], lons)}, 
                                   dims = ["lat", "lon"])

    print("Make long table...")
    out = []
    for lat, dt_lat in zip(tqdm(lats), smask):
        for lon, dt_lon in zip(lons, dt_lat):
            if mask_survey_areas and (smask.loc[{"lat": lat, "lon":lon}].values==0):
                continue
            else:
                for var in list(avg_ds.keys()):
                    row_res = {"lat": lat, "lon": lon, "year": "average"}
                    row_res["value"] =  avg_ds[var].sel(latitude = lat, longitude = lon, method = "nearest").values
                    row_res["variable"] = f"{var}"
                    out.append(row_res)
                
                for year in config.years:
                    yearly_rain = out_ds.sel(time = year) # sos,..
                    for var in list(yearly_rain.keys()):
                        row_res = {"lat": lat, "lon": lon}
                        row_res["value"] =  yearly_rain[var].sel(latitude = lat, longitude = lon, method = "nearest").values
                        row_res["variable"] = f"{var}_{year}"
                        row_res["year"] = year
                        out.append(row_res)

    res_df = pd.DataFrame(out)
    res_df.to_csv(os.path.join(config.weather_path, "weather_data_long.csv"))



def main():
    # calculate weather derivatives using hourly weather data
    yearly_ds, avg_ds = comp_rain_measures(config.y_strt, config.y_end, config.hourly_path)
    yearly_ds.to_netcdf(os.path.join(config.weather_path, "rain_season_dt.nc"))
    avg_ds.to_netcdf(os.path.join(config.weather_path, "rain_season_avg_dt.nc"))

    # load and combine monthly data
    monthly_era = xr.open_mfdataset(paths=os.path.join(config.monthly_path, "*.nc"))

    # get combined survey data
    survey_data = pd.read_csv(config.survey_path) 
    weather_comb = map_weather_to_locs(survey_data, config.years_to_map, monthly_era, yearly_ds, avg_ds)
    weather_comb.to_csv(os.path.join(config.weather_path, "era5_land_monthly_mapped.csv"))


if __name__ == "__main__":
    main()